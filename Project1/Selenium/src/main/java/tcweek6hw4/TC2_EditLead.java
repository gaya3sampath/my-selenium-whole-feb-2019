package tcweek6hw4;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethodsFirst;

public class TC2_EditLead extends ProjectMethodsFirst {
	
    //Find lead from TestLeaf and edit and Update that lead
	
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TC002_FL";
		testDesc ="Create a new lead in leaftaps";
		author ="Gayatri";
		category = "Smoke";
	}
	@Test(dependsOnGroups="TC1_CreateLead", alwaysRun=true)
	
	public void createLead() throws InterruptedException {
	clickWithOutSnap(locateElement("link", "CRM/SFA"));	
	clickWithOutSnap(locateElement("link", "Leads"));
	clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
	type(locateElement("xpath", "(//input[@name='firstName'])[3]"), "Gaya3");
	type(locateElement("xpath", "(//input[@name='lastName'])[3]"), "S");
	type(locateElement("xpath", "(//input[@name='companyName'])[2]"), "Htss");
	clickWithOutSnap(locateElement("xpath", "(//button[@class='x-btn-text'])[6]"));
	Thread.sleep(3000);
	clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
	clickWithOutSnap(locateElement("xpath", "(//a[@class='subMenuButton'])[3]"));
	selectDropDownUsingText(locateElement("xpath", "(//select[@class='inputBox'])[2]"), "Finance", "visible");
	clickWithOutSnap(locateElement("xpath", "(//input[@class='smallSubmit'])[1]"));

}
}