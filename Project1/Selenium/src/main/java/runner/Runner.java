package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(features= {"src/main/java/bddfeature/bdd.feature"},    //Feature File path
	glue = "implementation",                                           //Implementation Package name
						dryRun=false,                                  
						monochrome=true)                         //Using console window for exception message

public class Runner {

}
