package learnAnnotations;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LearnTestngAnnota {

	@BeforeSuite
	public void met1() {
		System.out.println("met1");
	}
		@BeforeTest
		public void met2() {
			System.out.println("met2");
		}
		@BeforeClass
		public void met3() {
			System.out.println("met3");
		}
		@BeforeMethod
		public void met4() {
			System.out.println("met4");
		}
		@AfterMethod
		public void met7() {
			System.out.println("met7");
		}
		//@Test
		public void met5() {
			System.out.println("met5");
			//throw new RuntimeException(); //Customzed created exception
		}
		
		//@Test
		public void met6() {
			System.out.println("met6");
		}
		@AfterClass
		public void met8() {
			System.out.println("met8");
		}
		@AfterTest
		public void met9() {
			System.out.println("met9");
		}
		@AfterSuite
		public void met10() {
			System.out.println("met10");
		}
	}
