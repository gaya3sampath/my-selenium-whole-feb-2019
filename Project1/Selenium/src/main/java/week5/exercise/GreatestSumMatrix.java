package week5.exercise;

import java.util.Scanner;

public class GreatestSumMatrix {

	public static void main(String[] args) {
		// TO 
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of row");
		int row =sc.nextInt();
		System.out.println("Enter the number of column");
		int col =sc.nextInt();
		int matrix[][] = new int[row][col];
		
		System.out.println("Enter the data of matrix ");
		for(int i=0; i<row; i++) {
			for(int j=0; j<col; j++) {
				matrix[i][j] =sc.nextInt();
				
			}
		}
		//To calculate the first Left diagonal
		int sumOfDiagonall = 0;
		for(int i=0, j=0; i<row && j<col; i++,j++) {
			sumOfDiagonall = sumOfDiagonall+ matrix[i][j];
		}
		// To calculate the second Right diagonal
		int sumOfDiagonalr=0;
		for(int i=0, j=col-1; i<row && j>=0; i++, j--) {
			sumOfDiagonalr = sumOfDiagonalr+matrix[i][j];
		}
       System.out.println("your matrix is ");
       for(int i=0; i<row; i++) {
    	   for(int j=0; j<col; j++) {
    		   System.out.print(matrix[i][j] +" ");
    	   }
    	   System.out.println("");
       }
       
       if(sumOfDiagonall > sumOfDiagonalr) {
    	   System.out.println("sum of greatest number across diagonal of matrix: " +sumOfDiagonall);
       }else
    	   System.out.println("sum of greatest number across diagonal of matrix: " +sumOfDiagonalr);
       
	}
   
}
