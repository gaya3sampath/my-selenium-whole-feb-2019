package week5.exercise;


public class SumOfMultiple {

	public static void main(String[] args) {
		// TO find the sum of all multiple of 3 or 5 less than 100
		//for eg: for 20, Multiple of 3 or 5 is 3,5,6,9,10,12,15,18=78
		
		//int sumof3 = 0;
		//int sumof5 = 0;
		//int repeat15 = 0; // multiple of 15 is repeating in multiple 3 or 5.so have to subtract it
		int sum = 0;
		for(int i = 1; i < 100; ++i)
		{
			if((i % 3 ==0) || (i % 5 ==0))
			{
				sum += i;
			}
		}
		System.out.println(sum);
		
		//for(int i =1; i<20; i++) {
			//if(i%3 == 0)
				//sumof3 +=i;
		//}
		//for(int i =1; i<20; i++) {
			//if(i%5 == 0)
				//sumof5 +=i;
	//}
		//for(int i =1; i<20; i++) {
			//if(i%15 == 0)
				//repeat15 +=i;
//}
        //System.out.println((sumof3 + sumof5) - repeat15);
        
	}
}
