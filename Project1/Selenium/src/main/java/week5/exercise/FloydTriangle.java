package week5.exercise;

import java.util.Scanner;

public class FloydTriangle {

	public static void main(String[] args) {
		// TO Print Floyd Triangle by getting number of rows 
		
		int n, num=1, c, d;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number of rows of Floyd's Tringle to display ");
		n = sc.nextInt();
		
		System.out.println("Floyd's triangle:");
		
		for (c = 1; c <= n; c++) {
			for (d = 1; d <= c; d++) {
				System.out.print(num+ " ");
				num++;
			}
			
			System.out.println();  //Moving to next row
		}
		
	}

}
