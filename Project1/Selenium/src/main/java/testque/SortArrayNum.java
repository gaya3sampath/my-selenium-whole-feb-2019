package testque;

import java.lang.reflect.Array;
import java.util.Arrays;

public class SortArrayNum {

	public static void main(String[] args) {
		// TO sort an array
		
		int [] arr = {30, 10, 15, 78, 45, 12, 30, 70};
		Arrays.sort(arr);
		
		for(int sortNum : arr) {
			
			System.out.println(sortNum);
		}
		
	}	

}

