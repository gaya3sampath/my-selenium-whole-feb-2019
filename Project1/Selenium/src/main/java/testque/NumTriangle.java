package testque;

public class NumTriangle {

	public static void main(String[] args) {
		// TO print the nuber in triangle pattern
		
		int lCount, num, i, j;
		lCount = 5;    // Layer count
		
		// outer loop to handle number of rows 
        //  n in this case
		for(i=0; i<lCount; i++) {
			 // initialising starting number 
			num=1;
			
		//  inner loop to handle number of columns 
            //  values changing acc. to outer loop
			for(j=0; j<=i; j++) {
				// printing num with a space 
				System.out.print(num+" ");
				
				//incrementing value of num 
				num++;
			}
			// ending line after each row 
		System.out.println();

	}

}
}