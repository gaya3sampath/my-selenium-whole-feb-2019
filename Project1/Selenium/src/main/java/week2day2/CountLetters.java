package week2day2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class CountLetters {

	public static void main(String[] args) {
		// To count each letter from the input
		
		System.out.println("Enter the String");
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		char[] ch = str.toCharArray();
		
		Map<Character, Integer> occurance = new LinkedHashMap<>();
		for (char c : ch) {
			if (occurance.containsKey(c)) {
				occurance.put(c, occurance.get(c)+1);
				
		}else	
		{
				occurance.put(c, 1);
			}
		
		}
		
	System.out.println(occurance);

	}
}
