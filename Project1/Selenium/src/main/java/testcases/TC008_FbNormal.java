package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TC008_FbNormal {

		// TO automate faceBook by normal method
		
		public static void main(String[] args) throws InterruptedException {
			// TO login fb
			System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			//disable Block window alert
			ChromeOptions op = new ChromeOptions();
			op.addArguments("--disable-notifications");
			ChromeDriver driver = new ChromeDriver(op);
			driver.get("https://www.facebook.com/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			
			driver.findElementByXPath("(//input[@class='inputtext'])[1]").sendKeys("7845221875");
			driver.findElementByXPath("(//input[@class='inputtext'])[2]").sendKeys("Surya@1993");
			driver.findElementByXPath("//input[@value='Log In']").click();
			
			driver.findElementByXPath("//input[@name='q']").sendKeys("TestLeaf",Keys.ENTER);
			String text1 = driver.findElementByXPath("//div[text()='TestLeaf']").getText();
			if (text1.equalsIgnoreCase("TestLeaf")) {
				System.out.println("we Found TestLeaf Page");
			}
			String text2 = driver.findElementByXPath("//button[contains(@class,'PageLikeButton ')]").getText();

			if (text2.equalsIgnoreCase("Like")) {
				driver.findElementByXPath("//button[contains(@class,'PageLikeButton ')]").click();
			} else if (text2.equalsIgnoreCase("Liked")) {
				System.out.println("Aleady Liked");
			}
			driver.findElementByXPath("//div[text()='TestLeaf']").click();
			if (driver.getTitle().equalsIgnoreCase("(65) TestLeaf - Home ")) {
				System.out.println("Title Matches");
			} else {
				System.out.println("Title not Matched");
			}

			String numberOfLikes = driver.findElementByXPath("//div[contains(text(),'people like this')]").getText().replaceAll("\\D", "");

			System.out.println("Total Likes : " + numberOfLikes);
			driver.quit();
			

		}

	}


