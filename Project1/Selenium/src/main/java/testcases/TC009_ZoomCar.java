package testcases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.SeMethods;

public class TC009_ZoomCar extends SeMethods{
	//Book car in ZoomCar application using SeMethods
	
	@Test
	public void LevelOne() throws InterruptedException{
		startApp("chrome", "https://www.zoomcar.com/chennai/");
		clickWithOutSnap(locateElement("linktext", "Start your wonderful journey"));
		clickWithOutSnap(locateElement("xpath", "//div[contains(text(),'Thuraipakkam')]"));
		clickWithOutSnap(locateElement("xpath", "//button[contains(text(),'Next')]"));
		String textStartD = getText(locateElement("xpath", "(//div[@class='days']/div)[2]"));
		clickWithOutSnap(locateElement("xpath", "(//div[@class='days']/div)[2]"));
		clickWithOutSnap(locateElement("xpath", "//button[contains(text(),'Next')]"));
		Thread.sleep(3000);
		String textEndD = getText(locateElement("xpath","(//div[@class='days']/div)[1]"));
		if(textStartD.equalsIgnoreCase(textEndD)) {
			System.out.println("Text is verified"+textStartD);
		}else {
			System.out.println("Text is verified"+textStartD);
		}
		clickWithOutSnap(locateElement("xpath", "//button[contains(text(),'Done')]"));
		Thread.sleep(3000);
		List<WebElement> mylist = driver.findElementsByXPath("//div[@class='price']");
		System.out.println(mylist.size());
		List<Integer> priceList = new ArrayList<Integer>();
		for(int i=0; i<mylist.size(); i++) {
		for(WebElement e: mylist) {
			//System.out.println(e.getText());
			String text = e.getText().replaceAll("\\D", "");
			System.out.println(text);
			priceList.add(Integer.parseInt(text));
			
		}
		System.out.println("Final List Size"+priceList.size());
		
		Collections.sort(priceList);
		Integer maxprice = Collections.max(priceList);
		System.out.println("Max value"+maxprice);
		String text = getText(locateElement("xpath","(//div[contains(text(),'"+maxprice+"')]/../../preceding-sibling::div)[2]/h3"));
		System.out.println("Result Car is:"+text);
		click(locateElement("xpath","(//div[contains(text(),'"+maxprice+"')])[1]/../button"));
		driver.quit();
		
	}
}
	}
