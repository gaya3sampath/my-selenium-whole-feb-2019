package implementation;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Implements {
	
	ChromeDriver driver;
	
	@Given("Open the Browser")
	public void open_the_Browser() {
	    System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
	    driver = new ChromeDriver();
	    
	}

	@Given("Load the URL")
	public void load_the_URL() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	    
	}

	@Given("Enter Username as DemoSalesManager")
	public void enter_Username_as_DemoSalesManager() {
	    driver.findElementById("username").sendKeys("DemoSalesManager");
	}

	@Given("Enter Password as crmsfa")
	public void enter_Password_as_crmsfa() {
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@Then("click submit button")
	public void click_submit_button() {
	   driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@Given("Select CRM")
	public void select_CRM() {
	   driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Select Leads")
	public void select_Leads() {
	    driver.findElementByLinkText("Leads").click();
	}

	@Given("Select Create Lead")
	public void select_Create_Lead() {
	    driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter Company Name as Hi Tech")
	public void enter_Company_Name_as_Hi_Tech() {
	   driver.findElementById("createLeadForm_companyName").sendKeys("Hi Tech");
	}

	@Given("Enter First Name as Gaya{int}")
	public void enter_First_Name_as_Gaya(Integer int1) {
	    driver.findElementById("createLeadForm_firstName").sendKeys("Gaya3");
	}
	

	@Given("Enter Last Name as S")
	public void enter_Last_Name_as_S() {
	    driver.findElementById("createLeadForm_lastName").sendKeys("S");
	}
	
	@Then("click Cl submit button")
	public void click_Cl_submit_button() {
	    driver.findElementByName("submitButton").click();
	}

}
