package ctsquestion;

public class DistinctEleArray {

	public static void main(String[] args) {
		// print distinct(Unique) element from an array
		
		//int[] nums = {5,2,7,2,4,7,8,2,3};
		String[] name = { "Java", "JavaScript", "Python", "C", "Ruby", "Java" };
		
		int length1 = name.length;
		
		for(int i=0;i<length1;i++){
            boolean isDistinct = false;
            for(int j=0;j<i;j++){
                if(name[i] == name[j]){
                    isDistinct = true;
                    break;
                }
            }
            if(!isDistinct){
                System.out.print(name[i]+" ");

	}

}
}
}