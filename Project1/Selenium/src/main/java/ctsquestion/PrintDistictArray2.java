package ctsquestion;

import java.util.HashSet;
import java.util.Set;

public class PrintDistictArray2 {

	public static void main(String[] args) {
		//Print distict element from an array - 2nd way
		
		//int[] nums = {5,2,7,2,4,7,8,2,3};
		String[] name = { "Java", "JavaScript", "Python", "C", "Ruby", "Java" };
				
				int length1 = name.length;
				
				Set<String> hs = new HashSet<String>();
				
				for(int i = 0; i<length1; i++)
				{
					hs.add(name[i]);
				}
			
				System.out.println("Distinct Elements");
				for (String i : hs) {
				    System.out.print(i+" ");
				}

	}

}
