package ctsquestion;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) throws InterruptedException {
		// TO automate a WebTable
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.creditmantri.com/gold-rate-chennai/");
		driver.manage().window().maximize();
		Thread.sleep(7000);
		
		WebElement table = driver.findElement(By.id("//*[@id=\"mm-0\"]/div[6]/div[2]/section[2]/div/div/div/div[1]/div[1]/div[3]/div/table/tbody"));
		 
	      // Get all rows (tr tags)
	      List<WebElement> rows = table.findElements(By.tagName("tr"));
	 
	      String Expected = "CellName";  
	      // Print data from each row (Data from each td tag)
	 
	      for (WebElement row : rows) {
	 
	      List<WebElement> cols = row.findElements(By.tagName("td"));
	 
	      for (WebElement col : cols) {
	 
	              System.out.print(col.getText() + "\t");
	 
	              String Actual = col.getText();                                               
	 
	              // Check Expected Cell is present or not in WebTable
	              if (Actual.equals(Expected)) {
	 
	                      System.out.println("Cell Exist in WebTable...");
	 
	                    }
	                }
	 
	            System.out.println();


	}

}
}
