package ctsquestion;

import java.util.HashMap;

public class CharOccur {

	public static void main(String[] args) {
		// to find character occurance
		
		String str = new String ("output file$");
		
		HashMap <Character, Integer> charcount = new HashMap<Character, Integer>();
		char[] ch = str.toCharArray();
		
		for (char c: ch) {
			if(charcount.containsKey(c)) {
				charcount.put(c, charcount.get(c)+1);
			}
			else
			{
				charcount.put(c,1);
			}
		}
          System.out.println(charcount);
	}

}
