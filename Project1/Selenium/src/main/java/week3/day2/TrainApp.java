package week3.day2;

import java.awt.RenderingHints.Key;
import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TrainApp {

	public static void main(String[] args) {
		
		//Print all train name in console
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS", Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MYS", Keys.TAB);
		WebElement check = driver.findElementByXPath("//input[@id='chkSelectDateOnly']");
		if (check.isSelected()) {
			check.click();
		}
		
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		for (int i=0; i<rows.size(); i++) {
                    WebElement eachrow = rows.get(i);
		List<WebElement> allcell = eachrow.findElements(By.tagName("td"));
		System.out.println(allcell.get(1).getText());
		
		}
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snap/image.png");
		File Util.copyFile(src, desc);
		
		
		
		
	}
}

