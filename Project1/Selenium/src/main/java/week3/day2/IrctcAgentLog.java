package week3.day2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IrctcAgentLog {

	public static void main(String[] args) {
		// In IRCTC page create Agent Login
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//Load the URL
		driver.get("http://irctc.co.in");
		//Maximise window screen
		driver.manage().window().maximize();
		driver.findElementByXPath("/html/body/app-root/app-home/app-header/div[2]/div[2]/div[1]/a[7]").click();
		//driver.findElementByXPath("//input[@id='usernameId']/following::td[1]").click();
		driver.findElementByXPath("//*[@id=\"loginFormId\"]/div[1]/div[2]/table[1]/tbody/tr[1]/td[3]/a").click();
		driver.findElementByXPath("//input[@name='userRegistrationForm:userName']").sendKeys("gayasampath");
		driver.findElementByXPath("//a[@id='userRegistrationForm:checkavail']").click();
		driver.findElementByXPath("//input[@id='userRegistrationForm:password']").sendKeys("gaya3Sampath");
		driver.findElementByXPath("//input[@id='userRegistrationForm:confpasword']").sendKeys("gaya3Sampath");
			
		WebElement sourcea = driver.findElementByXPath("//select[@class='labeltxt']");
		Select sca = new Select(sourcea);
		sca.selectByIndex(2);
		driver.findElementByXPath("//input[@id='userRegistrationForm:securityAnswer']").sendKeys("GHSS");
		driver.findElementByXPath("//input[@id='userRegistrationForm:firstName']").sendKeys("Preetha");
		//For Gender Radio button
		driver.findElementByXPath("//input[@id='userRegistrationForm:gender:1']").click();
		//For Marrital status radio button
		driver.findElementByXPath("//input[@id='userRegistrationForm:maritalStatus:1']").click();
		//Choose date for DOB
		WebElement sourceb = driver.findElementByXPath("//input[@id='userRegistrationForm:dobDay']");
		Select scb = new Select(sourceb);
		scb.selectByIndex(4);
		WebElement sourcec = driver.findElementByXPath("//select[@id='userRegistrationForm:dobMonth']");
		Select scc = new Select(sourcec);
		scc.selectByIndex(6);
		WebElement sourced = driver.findElementByXPath("//select[@id='userRegistrationForm:dateOfBirth']");
		Select scd = new Select(sourced);
		scd.selectByValue("1989");
		WebElement sourcee = driver.findElementByXPath("//select[@id='userRegistrationForm:occupation']");
		Select sce = new Select(sourcee);
		sce.selectByVisibleText("Student");
		//Select country
		WebElement sourcef = driver.findElementByXPath("//select[@id='userRegistrationForm:countries']");
		Select scf = new Select(sourcef);
		scf.selectByValue("94");
		//Select religion
		WebElement sourcen = driver.findElementByXPath("//select[@id='userRegistrationForm:nationalityId']");
		Select scn = new Select(sourcen);
		scn.selectByValue("94");
		//Input mail address
		driver.findElementByXPath("//select[@id='userRegistrationForm:email']").sendKeys("gaya11@hotmail.com");
		driver.findElementByXPath("//input[@id='userRegistrationForm:mobile']").sendKeys("944894903");
		driver.findElementByXPath("//input[@id='userRegistrationForm:address']").sendKeys("No126");
		driver.findElementByXPath("//input[@id='userRegistrationForm:pincode']").sendKeys("602001");
		Thread.sleep(2000);
		
		//driver.findElementByXPath("//input[@id='userRegistrationForm:statesName']").sendKeys("TAMIL NADU");
		WebElement sourceg = driver.findElementByXPath("//select[@id='userRegistrationForm:cityName']");
		Select scg = new Select(sourceg);
		scg.selectByValue("Tiruvallur");
		WebDriverWait wait = new WebDriverWait(driver,30);
		WebElement elePostOffice = wait.until(ExectedConditions.elementToBeClickable(By.id("userRegistrationForm:postofficeNameid")));
		WebElement sourceh = driver.findElementByXPath("//select[@id='userRegistrationForm:postofficeName']"); 
		Select sch = new Select(sourceh);
		sch.selectByVisibleText("Tiruvallur H.O");
		driver.findElementByXPath("//input[@id='userRegistrationForm:landline']").sendKeys("2435 6789");
		driver.findElementByXPath("//input[@id='userRegistrationForm:resAndOff:0']").click();
			
	}

}
