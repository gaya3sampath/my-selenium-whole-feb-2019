package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class ChromeLaunch {

	public static void main(String[] args) {
		// tell where is chrome driver available
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//Launch the URL
		driver.get("http://leaftaps.com/opentaps");
		//Maximise the window
		driver.manage().window().maximize();
		
	//WebElement eleUserName = driver.findElementById("username");
	//eleUserName.sendKeys("DemoSalesManager");
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Create Lead").click();
	driver.findElementById("createLeadForm_companyName").sendKeys("Hi Tech");
	driver.findElementById("createLeadForm_firstName").sendKeys("Peetha");
	driver.findElementById("createLeadForm_lastName").sendKeys("Rani");
	WebElement inputa = driver.findElementById("createLeadForm_dataSourceId");
	Select inpa = new Select(inputa);
	inpa.selectByVisibleText("Existing Customer");
	WebElement inputb = driver.findElementById("createLeadForm_marketingCampaignId");
	Select inpb = new Select(inputb);
	inpb.selectByValue("CATRQ_AUTOMOBILE");
	WebElement inputc = driver.findElementById("createLeadForm_industryEnumId");
	Select inpc = new Select(inputc);
	inpc.selectByIndex(3);
	/*List<WebElement> inp = scb.getOptions();
	for (WebElement ele : inp) {
		if(ele.getText().startsWith("C"))
			System.out.println(ele.getText());*/
	driver.findElementById("createLeadForm_firstNameLocal").sendKeys("pree");
	driver.findElementById("createLeadForm_lastNameLocal").sendKeys("rani");
	driver.findElementById("createLeadForm_personalTitle").sendKeys("Aaaa");
	driver.findElementById("createLeadForm_departmentName").sendKeys("Quality");
	driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Bbbb");
	driver.findElementById("createLeadForm_annualRevenue").sendKeys("1000");
	WebElement inputd = driver.findElementById("createLeadForm_ownershipEnumId");
	Select inpd = new Select(inputd);
	inpd.selectByValue("OWN_PARTNERSHIP");
	driver.findElementById("createLeadForm_numberEmployees").sendKeys("2000");
	WebElement moneyval = driver.findElementById("createLeadForm_currencyUomId");
	Select monval = new Select(moneyval);
	monval.selectByValue("INR");
	driver.findElementById("createLeadForm_sicCode").sendKeys("123");
	driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Tick");
	driver.findElementByName("description").sendKeys("Revenue is in dollor, no of employees are 2000");
	driver.findElementById("createLeadForm_importantNote").sendKeys("ABCDEF45XYZ");
	driver.findElementByClassName("inputBox").sendKeys("+91");
	driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
	driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("999123456");
	driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("2444 4838");
	driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("9447654321");
	driver.findElementById("createLeadForm_primaryEmail").sendKeys("hitech@yahoo.com");
	driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.hitech.com");
	driver.findElementById("createLeadForm_generalToName").sendKeys("Kkkkk");
	driver.findElementById("createLeadForm_generalAttnName").sendKeys("Ccccc");
	driver.findElementById("createLeadForm_generalAddress1").sendKeys("Main Steet");
	driver.findElementById("createLeadForm_generalAddress2").sendKeys("Rrrrr");
	driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
	WebElement inpute = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
	Select inpe = new Select(inpute);
	inpe.selectByVisibleText("Indiana");
	driver.findElementById("createLeadForm_generalPostalCode").sendKeys("602003");
	WebElement inputf = driver.findElementById("createLeadForm_generalCountryGeoId");
	Select inpf = new Select(inputf);
	inpf.selectByVisibleText("India");
	driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("600017");
	driver.findElementByName("submitButton").click();
		
	}
	
	}


