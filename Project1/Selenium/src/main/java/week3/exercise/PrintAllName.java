package week3.exercise;


import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class PrintAllName {

	public static void main(String[] args) {
		// TO print all the country name from drop down 
		
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load the Url
		driver.get("http://irctc.co.in");
		//Maximise the Window
		driver.manage().window().maximize();
		//driver.findElementByXPath("(//span[@class='ng-star-inserted'])[1]").click();
		driver.findElementByXPath("/html/body/app-root/app-home/app-header/div[2]/div[2]/div[1]/a[7]").click();
		
		driver.findElementByXPath("//*[@id=\"loginFormId\"]/div[1]/div[2]/table[1]/tbody/tr[1]/td[3]/a").click();
		
		WebElement eleCountry = driver.findElementByXPath("//select[@id='userRegistrationForm:countries']");
        Select country = new Select(eleCountry);
		
		List<WebElement> countryList = country.getOptions();
		for(WebElement country1:countryList)
		{
			System.out.println(country1.getText());
		}
		
		//skip  -- Select a Country --  
		for (int j = 1; j < countryList.size(); j++) {
	        System.out.println(countryList.get(j).getText());
		}
			

	}

}
