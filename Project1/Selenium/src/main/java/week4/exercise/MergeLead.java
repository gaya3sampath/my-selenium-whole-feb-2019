package week4.exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		// Y181103 program - Merge lead in Test Leaf
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		//Launch the URL
		driver.get("http://leaftaps.com/opentaps");
		//Maximise the window
		driver.manage().window().maximize();
		
    driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText("Merge Leads").click();
	driver.findElementByXPath("//a[@class='buttonDangerous']/preceding::img[2]").click();
	Set<String> newwindow1 = driver.getWindowHandles();
	List<String> listofWindow1 = new ArrayList<>();
	listofWindow1.addAll(newwindow1);
	//Focus to 'From Lead' window
	driver.switchTo().window(listofWindow1.get(1));
	//Maximise the 2nd window'
	driver.manage().window().maximize();
	Thread.sleep(3000);
	driver.findElementByName("id").sendKeys("10136");
	driver.findElementByXPath("(//td[@class='x-btn-center'])[1]").click();
	Thread.sleep(5000);
	driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	//driver.close();
	Set<String> primewindow = driver.getWindowHandles();
	List<String> allwindows1 = new ArrayList<>();
	allwindows1.addAll(primewindow);
	//Focus to Primary window
	driver.switchTo().window(allwindows1.get(0));
	Thread.sleep(3000);
	driver.findElementByXPath("//a[@class='buttonDangerous']/preceding::img[1]").click();
	Set<String> newwindow2 = driver.getWindowHandles();
	List<String> listofWindow2 = new ArrayList<>();
	listofWindow2.addAll(newwindow2);
	//Focus to 'To Lead' window
	driver.switchTo().window(listofWindow2.get(1));
	driver.manage().window().maximize();
	Thread.sleep(3000);
	driver.findElementByName("id").sendKeys("10506");
	driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
	Thread.sleep(5000);			
	driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
	Set<String> primewindow1 = driver.getWindowHandles();
	List<String> allwindows2 = new ArrayList<>();
	allwindows2.addAll(primewindow1);
	//Focus to Primary window
	System.out.println(allwindows2.size());
	driver.switchTo().window(allwindows1.get(0));
	System.out.println(driver.getCurrentUrl());
	driver.findElementByXPath("//a[@class='buttonDangerous']").click();
	driver.switchTo().alert().accept();
	Thread.sleep(5000);
	driver.findElementByLinkText("Find Leads").click();
	
	
		
	
	}

}
