package week1;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TO find factorial of given number
		
		int num, fact=1;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Factorial Number");
		num = sc.nextInt();
		
		for(int i = 1; i<=num; i++) {
			
			fact = fact*i;
			
		}
		
         System.out.println("Factorial of given number is " + fact);
	}

}
