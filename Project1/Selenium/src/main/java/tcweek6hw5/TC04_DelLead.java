package tcweek6hw5;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC04_DelLead extends ProjectMethods  {
	
	//Find lead from TestLeaf and Delete the lead
	//set
	
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TC04_DelLead";
		testDesc ="This test case for Delete the Lead";
		author ="Gayatri";
		category = "Smoke";
		excelfilename = "editlead";
	}
	@Test(dataProvider = "fetchData4")
	
	public void createLead(String fname4, String lName4, String cName4) throws InterruptedException {
	clickWithOutSnap(locateElement("link", "CRM/SFA"));	
	clickWithOutSnap(locateElement("link", "Leads"));
	clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
	type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fname4);
	type(locateElement("xpath", "(//input[@name='lastName'])[3]"), lName4);
	type(locateElement("xpath", "(//input[@name='companyName'])[2]"), cName4);
	clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[6]"));
	Thread.sleep(3000);
	clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
	clickWithOutSnap(locateElement("xpath", "//a[text()='Delete']"));
				
	}

}
