package tcweek6hw5;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC02_EditLead extends ProjectMethods {
	
	 //Find lead from TestLeaf and edit and Update that lead
	
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TC02_EditLead";
		testDesc ="Edit the lead  from FindLead";
		author ="Gayatri1";
		category = "Smoke";
		excelfilename = "editlead";
	}
	
	@Test(dataProvider= "fetchData" )
	
	public void createLead(String fName2, String lName2, String cName2, String industry2) throws InterruptedException {
	clickWithOutSnap(locateElement("link", "CRM/SFA"));	
	clickWithOutSnap(locateElement("link", "Leads"));
	clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
	type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName2);
	type(locateElement("xpath", "(//input[@name='lastName'])[3]"), lName2);
	type(locateElement("xpath", "(//input[@name='companyName'])[2]"), cName2);
	clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[6]"));
	Thread.sleep(3000);
	clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
	clickWithOutSnap(locateElement("xpath", "(//a[@class='subMenuButton'])[3]"));
	selectDropDownUsingText(locateElement("xpath", "(//select[@class='inputBox'])[2]"), industry2, "visible");
	clickWithOutSnap(locateElement("xpath", "(//input[@class='smallSubmit'])[1]"));
			
	}

}
