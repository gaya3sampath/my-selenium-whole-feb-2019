package tcweek6hw5;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC01_Createlead extends ProjectMethods  {
	
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TC1_CreateLead";
		testDesc ="Create a new lead in leaftaps";
		author ="Gayatri4-1"; 
		category = "Smoke";
		excelfilename = "createlead";
	}
	
	@Test(groups = {"smoke"}, dataProvider = "fetchData")
	
	public void createLead(String cName, String fName, String lName) {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Create Lead"));
		type(locateElement("id", "createLeadForm_companyName"), cName);
		type(locateElement("id", "createLeadForm_firstName"), fName);
		type(locateElement("id", "createLeadForm_lastName"), lName);
		type(locateElement("id", "createLeadForm_primaryEmail"), "gaya3@yahoo.com");
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), "9444894901");
		click(locateElement("name", "submitButton"));
	}
	
}

