package tcweek6hw5;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC03_DupliLead extends ProjectMethods  {
	//Find lead duplicate the lead in from Leads tab
		
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TC03_DupliLead";
		testDesc ="Create a new lead in leaftaps";
		author ="Gayatri";
		category = "Smoke";
		excelfilename = "duplicatelead";
		
	}
	@Test(dataProvider = "fetchData3")
	
	public void createLead(String fName3, String lName3, String cName3, String ownership3) throws InterruptedException {
	clickWithOutSnap(locateElement("link", "CRM/SFA"));	
	clickWithOutSnap(locateElement("link", "Leads"));
	clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
	type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName3);
	type(locateElement("xpath", "(//input[@name='lastName'])[3]"), lName3);
	type(locateElement("xpath", "(//input[@name='companyName'])[2]"), cName3);
	clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[6]"));
	Thread.sleep(3000);
	clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
	clickWithOutSnap(locateElement("xpath", "(//a[@class='subMenuButton'])[1]"));
	Thread.sleep(3000);
	selectDropDownUsingText(locateElement("xpath", "//select[@name='ownershipEnumId']"), ownership3, "visible");
	clickWithOutSnap(locateElement("xpath", "//input[@class='smallSubmit']"));
	
	}

}
