package tesatcasesweek6;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC005_DuplicateLead extends ProjectMethods {
	
	//Find lead duplicate the lead in from Leads tab
	
			@BeforeClass(groups = "config")
			public void setData() {
				testcaseName ="TC005_DuplicateLead";
				testDesc ="Create a new lead in leaftaps";
				author ="Gayatri";
				category = "Smoke";
			}
			@Test(dataProvider = "fetchData3", priority=3)
			
			public void createLead(String fName3, String lName3, String cName3, String ownership3) throws InterruptedException {
			clickWithOutSnap(locateElement("link", "CRM/SFA"));	
			clickWithOutSnap(locateElement("link", "Leads"));
			clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
			type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName3);
			type(locateElement("xpath", "(//input[@name='lastName'])[3]"), lName3);
			type(locateElement("xpath", "(//input[@name='companyName'])[2]"), cName3);
			clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[6]"));
			Thread.sleep(3000);
			clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
			clickWithOutSnap(locateElement("xpath", "(//a[@class='subMenuButton'])[1]"));
			Thread.sleep(3000);
			selectDropDownUsingText(locateElement("xpath", "//select[@name='ownershipEnumId']"), ownership3, "visible");
			clickWithOutSnap(locateElement("xpath", "//input[@class='smallSubmit']"));
			
			}
			
			@DataProvider(name = "fetchData3")
			public  String[][] getData() {
				String[][] data3 = new String[2][4];
				data3[0][0] = "Gaya3";
				data3[0][1] = "S";
				data3[0][2] = "Htss";
				data3[0][3] = "Partnership";
				
				data3[1][0] = "Pree";
				data3[1][1] = "T";
				data3[1][2] = "HiTech";
				data3[1][3] = "Public Corporation";
				
				return data3;
			
			
		}


}