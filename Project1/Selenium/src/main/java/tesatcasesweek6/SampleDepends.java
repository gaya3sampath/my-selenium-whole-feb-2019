package tesatcasesweek6;

import org.testng.annotations.Test;

public class SampleDepends {

	@Test
	public static void CreateLed() {
		// This test case is an example for dependsOn methods 
		System.out.println("CreateLead");

	}

	@Test(dependsOnMethods = "CreateLed")
	public void EditLead() {
		System.out.println("EditLead");
	}
	
	@Test(dependsOnMethods = "CreateLad")
	public void DuplicateLead() {
		System.out.println("DuplicateLead");
	}
	
	@Test
	public void DeletLead() {
		System.out.println("DeleteLead");
		
	}
}
