package tesatcasesweek6;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods {

	  //Find lead from TestLeaf and edit and Update that lead
		
		@BeforeClass(groups = "config")
		public void setData() {
			testcaseName ="TC004_EditLead";
			testDesc ="Edit the lead  from FindLead";
			author ="Gayatri1";
			category = "Smoke";
		}
		
		@Test(dataProvider= "fetchData2", priority=2 )
		
		public void createLead(String fName2, String lName2, String cName2, String industry2) throws InterruptedException {
		clickWithOutSnap(locateElement("link", "CRM/SFA"));	
		clickWithOutSnap(locateElement("link", "Leads"));
		clickWithOutSnap(locateElement("xpath", "//a[text()='Find Leads']"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName2);
		type(locateElement("xpath", "(//input[@name='lastName'])[3]"), lName2);
		type(locateElement("xpath", "(//input[@name='companyName'])[2]"), cName2);
		clickWithOutSnap(locateElement("xpath", "(//td[@class='x-btn-center'])[6]"));
		Thread.sleep(3000);
		clickWithOutSnap(locateElement("xpath", "(//a[@class='linktext'])[4]"));
		clickWithOutSnap(locateElement("xpath", "(//a[@class='subMenuButton'])[3]"));
		selectDropDownUsingText(locateElement("xpath", "(//select[@class='inputBox'])[2]"), industry2, "visible");
		clickWithOutSnap(locateElement("xpath", "(//input[@class='smallSubmit'])[1]"));
				
		}
		
		@DataProvider(name = "fetchData2")
		public  String[][] getData() {
			String[][] data2 = new String[2][4];
			data2[0][0] = "Gaya3";
			data2[0][1] = "S";
			data2[0][2] = "Htss";
			data2[0][3] = "Finance";
			
			data2[1][0] = "Pree";
			data2[1][1] = "T";
			data2[1][2] = "HiTech";
			data2[1][3] = "Insurance";
			
			return data2;
		}	
		
		
	}
	
	

