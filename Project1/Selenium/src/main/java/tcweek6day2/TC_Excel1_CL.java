package tcweek6day2;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC_Excel1_CL extends ProjectMethods  {
	
	@BeforeClass(groups = "config")
	public void setData() {
		testcaseName ="TC001_CreateLead";
		testDesc ="Create a new lead in leaftaps";
		author ="Gayatri";
		category = "Smoke";
		excelfilename = "createlead";
	}
	@Test(groups = {"smoke"}, dataProvider = "fetchData")
		
	public void createLead(String cName, String fName, String lName) {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Create Lead"));
		type(locateElement("id", "createLeadForm_companyName"), cName);
		type(locateElement("id", "createLeadForm_firstName"), fName);
		type(locateElement("id", "createLeadForm_lastName"), lName);
		type(locateElement("id", "createLeadForm_primaryEmail"), "gaya3@yahoo.com");
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"), "9444894901");
		click(locateElement("name", "submitButton"));
	}
	
	//@Dataprovider is common for all operations, so you can put it in ProjectMethods
	
	/*@DataProvider(name = "fetchData")
	public  String[][] getData() throws IOException {
		return GetExcelData.getExcelData("createlead");*/ // call the excel data from that class classname.getExcelData("filename")
		
		/*String[][] data = new String[2][3];
		data[0][0] = "Htss";
		data[0][1] = "Gaya3";
		data[0][2] = "S";
		
		data[1][0] = "HiTech";
		data[1][1] = "Pree";
		data[1][2] = "T";
		
		return data;*/
	}
	

