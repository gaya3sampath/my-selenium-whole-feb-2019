package tcweek6day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GetExcelData {

	public static String [][] getExcelData(String excelFileName) throws IOException {
		// TO Get data from the excel sheet
		
		
		XSSFWorkbook myBook = new XSSFWorkbook("./data/"+excelFileName+".xlsx");
		XSSFSheet sheet = myBook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println("row count : " +rowCount);
		short colCount = sheet.getRow(0).getLastCellNum();
		System.out.println("col count: " +colCount);
		String[][] data = new String[rowCount][colCount];
		
		for (int i = 1; i <=rowCount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < colCount; j++) {
				XSSFCell cell = row.getCell(j);
				String cellValue = cell.getStringCellValue();
				data[i-1][j] = cellValue;                   //i-1 is fetch the value in 0 row
				System.out.println(cellValue);
			} 
		}
		return data;
		

		
	}

}
