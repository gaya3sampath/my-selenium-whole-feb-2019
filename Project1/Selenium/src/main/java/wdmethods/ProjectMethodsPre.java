package wdmethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class ProjectMethodsPre extends SeMethods {
	
	// In this interacting with Excel function is not created

	@BeforeMethod(groups = "config")
	@Parameters({"browser", "url", "username", "password"})
	public void login(String browser, String Url, String uname, String pword) {
		
	startApp(browser, Url);
	WebElement eleUsername = locateElement("id", "username");
	type(eleUsername, uname);
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, pword);
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin); 

}
	@AfterMethod(groups = "config")
	public void closeApp() {
		closeBrowser();
	}

	}


