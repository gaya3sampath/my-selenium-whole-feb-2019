package wdmethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class ProjectMethodsFirst extends SeMethods {
	
	@BeforeMethod(groups = "config")
	public void login() {
		
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUsername = locateElement("id", "username");
	type(eleUsername, "DemoCSR");
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin); 

}
	@AfterMethod(groups = "config")
	public void closeApp() {
		closeBrowser();
	}
}
