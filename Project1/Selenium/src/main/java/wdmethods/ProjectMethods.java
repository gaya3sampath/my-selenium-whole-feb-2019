package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import tcweek6day2.GetExcelData;


public class ProjectMethods extends SeMethods{
	
	@DataProvider(name = "fetchData")
	public  String[][] getData() throws IOException {
		return GetExcelData.getExcelData(excelfilename);
	}
	
	@BeforeMethod(groups = "config")
	@Parameters({"browser", "url", "username", "password"})
	public void login(String browser, String Url, String uname, String pword) {
		
	startApp(browser, Url);
	WebElement eleUsername = locateElement("id", "username");
	type(eleUsername, uname);
	WebElement elePassword = locateElement("id", "password");
	type(elePassword, pword);
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin); 

}
	@AfterMethod(groups = "config")
	public void closeApp() {
		closeBrowser();
	}
}


