package wee4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertTryIt {

	public static void main(String[] args) {
		// To select try and input in alert dialog and 
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		driver.switchTo().alert().sendKeys("Gayathri");
		driver.switchTo().alert().accept();
		String text = driver.findElementById("demo").getText();
		
		if(text.contains("azf")){
			
			System.out.println(text);
		}else
			
			System.out.println("Invalid message");
		//Chrome automatically close
		driver.close();
		
		
		
		
		
		
		
		
	}

}
