Feature: Leaftaps
Background:
Given Launch the Browser
And Load the Url
And Maximize the window
And Set Wait

Scenario: Login Leaftaps
When Enter Username as DemoSalesManager
And Enter Password as crmsfa
And click Login
Then Verify success
And Logout
And Close Browser

Scenario Outline: Multiple login
When Enter Username as <username>
And Enter Password as <password>
And click Login
Then Verify success
And Logout
And Close Browser

Examples:
|username|password|
|DemoSalesManager|crmsfa|
|DemoCSR|crmsfa|




